#!/bin/bash

# Check if the correct number of arguments is provided
if [[ $# -lt 2 ]]; then
  echo "Usage: $0 <dockershareddir> <assembly (GRCh37, GRCh38, or both)>"
  exit 1
fi

# Set variables
dockershareddir=$1
assembly=$2 # GRCh37, GRCh38, or both

# VEP cache download and processing
# Ensure the shared directory exists
mkdir -p "${dockershareddir}/.vep"

# Install VEP cache for GRCh37 and GRCh38 based on the assembly argument
if [[ "$assembly" == "GRCh37" || "$assembly" == "both" ]]; then
  perl INSTALL.pl --AUTO cfp \
  --NO_UPDATE \
  --SPECIES homo_sapiens_refseq \
  --ASSEMBLY GRCh37 \
  --CACHEDIR "${dockershareddir}/.vep" \
  --CACHE_VERSION 105 \
  -g dbscSNV,LoFtool,ExACpLI,dbNSFP,MaxEntScan,CADD

  # Unzip and re-compress the reference genome for GRCh37
  zcat "${dockershareddir}/.vep/homo_sapiens_refseq/105_GRCh37/Homo_sapiens.GRCh37.75.dna.primary_assembly.fa.gz" > \
  "${dockershareddir}/.vep/homo_sapiens_refseq/105_GRCh37/Homo_sapiens.GRCh37.75.dna.primary_assembly.fa"
  bgzip --force "${dockershareddir}/.vep/homo_sapiens_refseq/105_GRCh37/Homo_sapiens.GRCh37.75.dna.primary_assembly.fa"
fi

if [[ "$assembly" == "GRCh38" || "$assembly" == "both" ]]; then
  perl INSTALL.pl --AUTO cfp \
  --NO_UPDATE \
  --SPECIES homo_sapiens_refseq \
  --ASSEMBLY GRCh38 \
  --CACHEDIR "${dockershareddir}/.vep" \
  --CACHE_VERSION 105 \
  -g dbscSNV,LoFtool,ExACpLI,dbNSFP,MaxEntScan,CADD

  zcat "${dockershareddir}/.vep/homo_sapiens_refseq/105_GRCh28/Homo_sapiens.GRCh28.75.dna.primary_assembly.fa.gz" > \
  "${dockershareddir}/.vep/homo_sapiens_refseq/105_GRCh28/Homo_sapiens.GRCh28.75.dna.primary_assembly.fa"
  bgzip --force "${dockershareddir}/.vep/homo_sapiens_refseq/105_GRCh28/Homo_sapiens.GRCh28.75.dna.primary_assembly.fa"
fi

# Custom directory setup and data processing
mkdir -p "${dockershareddir}/custom"
cd "${dockershareddir}/custom"

# Download and process dbNSFP
wget ftp://dbnsfp:dbnsfp@dbnsfp.softgenetics.com/dbNSFP4.3a.zip
unzip dbNSFP4.3a.zip

# Extract the header from the first chromosome file
zcat dbNSFP4.3a_variant.chr1.gz | head -n1 > header.txt

# Process dbNSFP data for GRCh37 and GRCh38
if [[ "$assembly" == "GRCh37" || "$assembly" == "both" ]]; then
  mkdir -p tmp_sort/
  zgrep -h -v ^#chr dbNSFP4.3a_variant.chr* | \
  awk '$8 != "."' | \
  sort -T tmp_sort/ -k8,8 -k9,9n | \
  cat header.txt - | \
  bgzip -c > dbNSFP4.3a_grch37.gz
  tabix -s 8 -b 9 -e 9 dbNSFP4.3a_grch37.gz
  # Uncomment the following line to clean temporary files
  # rm -r tmp_sort/
fi

if [[ "$assembly" == "GRCh38" || "$assembly" == "both" ]]; then
  mkdir -p tmp_sort/
  zgrep -h -v ^#chr dbNSFP4.3a_variant.chr* | \
  sort -T tmp_sort/ -k1,1 -k2,2n | \
  cat header.txt - | \
  bgzip -c > dbNSFP4.3a_grch38.gz
  tabix -s 1 -b 2 -e 2 dbNSFP4.3a_grch38.gz
  # Uncomment the following line to clean temporary files
  # rm -r tmp_sort/
fi

# Process dbNSFP gene data
zcat dbNSFP4.3_gene.complete.gz | \
cut -f1,14,20,26,27,30,31,32,33,36,37,95 > dbNSFP4.3_gene.complete.pvm.txt

# Additional cleanup (if necessary)
# Uncomment the following lines to clean redundant files
# rm dbNSFP4.3a_variant.chr* header.txt dbNSFP4.3a.zip dbNSFP4.3_gene.gz dbNSFP4.3_gene.complete.gz
