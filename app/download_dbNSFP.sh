#!/bin/bash

# Check if the correct number of arguments is provided
if [[ $# -lt 2 ]]; then
  echo "Usage: $0 <dockershareddir> <assembly (GRCh37, GRCh38, or both)>"
  exit 1
fi

# Set variables
dockershareddir=$1
assembly=$2 # GRCh37 or GRCh38 or both
shared_dir="${dockershareddir}/custom"

# Create necessary directories and navigate to the custom folder
mkdir -p "${shared_dir}"
cd "${shared_dir}"

# Download dbNSFP
# dbNSFP is a comprehensive database of functional prediction and annotation for all potential non-synonymous single-nucleotide variants (nsSNVs) in the human genome.
wget ftp://dbnsfp:dbnsfp@dbnsfp.softgenetics.com/dbNSFP4.3a.zip
unzip dbNSFP4.3a.zip

# Extract the header from the first chromosome variant file
zcat dbNSFP4.3a_variant.chr1.gz | head -n1 > header.txt

# If assembly is GRCh37 or both, process the GRCh37 data
if [[ "$assembly" == "GRCh37" || "$assembly" == "both" ]]; then
  mkdir -p tmp_sort/
  zgrep -h -v ^#chr dbNSFP4.3a_variant.chr* | \
  awk '$8 != "."' | \
  sort -T tmp_sort/ -k8,8 -k9,9n | \
  cat header.txt - | \
  bgzip -c > dbNSFP4.3a_grch37.gz
  tabix -s 8 -b 9 -e 9 dbNSFP4.3a_grch37.gz
  # rm -r tmp_sort/ # Uncomment to clean temporary directory
fi

# If assembly is GRCh38 or both, process the GRCh38 data
if [[ "$assembly" == "GRCh38" || "$assembly" == "both" ]]; then
  mkdir -p tmp_sort/
  zgrep -h -v ^#chr dbNSFP4.3a_variant.chr* | \
  sort -T tmp_sort/ -k1,1 -k2,2n | \
  cat header.txt - | \
  bgzip -c > dbNSFP4.3a_grch38.gz
  tabix -s 1 -b 2 -e 2 dbNSFP4.3a_grch38.gz
  # rm -r tmp_sort/ # Uncomment to clean temporary directory
fi

# Create the gene information file from the compressed gene data
zcat dbNSFP4.3_gene.complete.gz | \
cut -f1,14,20,26,27,30,31,32,33,36,37,95 > dbNSFP4.3_gene.complete.pvm.txt
