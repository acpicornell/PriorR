#!/usr/bin/env python
# coding: utf-8

import pandas as pd
import argparse
import time

# Set up argument parsing
parser = argparse.ArgumentParser(description='FJD CNV database step1')
parser.add_argument('-f', '--file', help='WGS variant file (TSV)', required=True)
parser.add_argument('-c', '--canonical', help='Filter for canonical transcripts', action='store_true')
parser.add_argument('-af', '--frequency', help='Maximum allele frequency to filter', type=float)
parser.add_argument('-r', '--region', help='Filter for specific genomic regions', action='store_true')
parser.add_argument('-p', '--panel', help='Virtual panel of genes for filtering', required=False)

# Start measuring time
start_time = time.time()

# Parse arguments
args = parser.parse_args()

# Load the TSV file into a DataFrame
df = pd.read_csv(args.file, sep='\t')

# Apply canonical filter if specified
if args.canonical:
    df = df[df['CANONICAL'] == 'YES']

# Apply genomic region filter if specified
if args.region:
    df = df[(df['Genomic_region'] == 'EXONIC') | (df['Genomic_region'] == 'SPLICING')]

# Apply allele frequency filter if specified
if args.frequency is not None:
    df = df[(df['gnomADg_AF_popmax'] <= args.frequency) | df['gnomADg_AF_popmax'].isna()]

# Apply gene panel filter if specified
if args.panel:
    with open(args.panel) as f:
        genes = f.read().splitlines()  # Read the list of genes from the panel file
    df = df[df['SYMBOL'].isin(genes)]  # Keep only rows with genes in the panel

# Output the filtered DataFrame to a TSV file
output_file = "/home/raquel/PriorR/tmp/wgs.tsv"
df.to_csv(output_file, sep='\t', index=False)

# Print elapsed time
print(f"Execution time: {time.time() - start_time:.2f} seconds")
