# Base image to build upon
FROM condaforge/mambaforge:4.12.0-2

# Documentation for this Docker image
LABEL authors="Raquel Romero" \
      description="PriorR"

# Install conda environment and clean up
COPY environment.yml /
RUN mamba env create -f /environment.yml && conda clean -a

# Add conda environment to PATH
ENV PATH="/opt/conda/envs/PriorR/bin:${PATH}"

# Record the installed conda environment for reference
RUN conda env export --name minimal_docker > minimal_docker.yml

# Install required system packages
RUN apt-get update && apt-get install -y \
  gcc \
  g++ \
  make \
  perl \
  cpanminus \
  libmysqlclient-dev \
  unzip \
  libbz2-dev \
  liblzma-dev \
  bcftools \
  tabix \
  bzip2 \
  bedtools

# Install Perl packages via cpanm
RUN cpanm Archive::Zip DBD::mysql LWP::Simple

# Download, unzip, and install Ensembl VEP
RUN wget https://github.com/Ensembl/ensembl-vep/archive/release/105.zip && unzip 105.zip
WORKDIR ensembl-vep-release-105
RUN perl INSTALL.pl -a a --NO_UPDATE --CACHE_VERSION 105

# Set permissions and add AutoMap
ADD AutoMap/ /home/docker/AutoMap/
RUN chmod 777 /home/docker

# Copy application files into the Docker image
COPY app/ /home/app/
COPY session/app/ /session/app/

# Set the working directory to the root
WORKDIR /

# Expose port for the application
EXPOSE 8888

# Command to start the application
CMD R -e "shiny::runApp('/home/app/PRIORR3.1_docker.R', host='0.0.0.0', port=8888)"
