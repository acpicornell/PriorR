# PriorR

Priorr is a prioritization program of disease-linked genetic variants devoloped within the Genetics&Genomics Department of La Fundacion Jimenez Diaz University Hospital. Priorr is conceived to analyse the output of the FJD-pipeline of SNVs or CNVs. This software program offers a number of useful functionalities for variant analysis such as: filtering by a virtual panel of genes. manual control of different population frequencies or pathogenicity predictors or filtering out variants that have been already found by another protocol.  

# Developers
## Main developer
 - Raquel Romero Fernández

## Collaborators
 - Gonzalo Núñez Moreno


# License

VariantCallingFJD source code is provided under the [**Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)**](https://creativecommons.org/licenses/by-nc-sa/4.0/). VariantCallingFJD includes several third party packages provided under other open source licenses, please check them for additional details.

[![Licencia de Creative Commons](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc-sa/4.0/)


# Installation and execution


PriorR is available at dockerhub, instructions for installation and execution can be found in https://hub.docker.com/r/tblabfjd/priorr.


